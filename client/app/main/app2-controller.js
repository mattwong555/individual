(function () {
    angular
        .module("uirouterApp")
        .controller("AppCtrl2", ["$state", AppCtrl2]);

    function AppCtrl2( $state){
        var vm = this;
        console.log("App controller 2--> ");

        vm.gogo = gogo;

        function gogo(){
            $state.go('F');
        }
    }

})();