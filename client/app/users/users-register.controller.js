(function() {
    angular
        .module("uirouterApp")
        .controller("RegisterCtrl", RegisterCtrl);

    RegisterCtrl.$inject = ['$state', 'AuthService'];

    function RegisterCtrl($state, AuthService) {

        var vm = this;

        // users={};

        vm.users = {
                    username: '',
                    password: ''};

        vm.register = register;

        function register(user,password) {
            // console.log(JSON.stringify(vm.users))
            console.log("Register ctrl user: ",user)
            console.log("Register ctrl password: ",password)
            // console.log(JSON.stringify(password))
            // console.log(JSON.stringify(vm.users.password))
            // console.log("inside register ctrl")
            AuthService
                .register(user, password)
                .then(function(result) {
                    $state.go("SignIn");
                })
                .catch(function(err) {
                    console.log("reg ctrl err" + err)
                })
        }

    }
})();