(function() {
    angular
        .module("uirouterApp")
        .controller("LoginCtrl", LoginCtrl);

    LoginCtrl.$inject = ['$state', '$http', 'AuthService'];

    function LoginCtrl($state, AuthService) {
        var vm = this;

        vm.login = login;

        function login() {


            AuthService.login(vm.user)
                .then(function(result) {


                }).catch(function() {

                    console.error("Error logging on !");
                });
        };
    }
})();