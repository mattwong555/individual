(function() {
    angular
        .module("uirouterApp")
        .controller("ListQnCtrl", ListQnCtrl);

    ListQnCtrl.$inject = ['$state', 'PeopleService'];

    function ListQnCtrl($state, PeopleService) {
        var vm = this;

        vm.question_text = "";
        vm.listQn = listQn;
        vm.questionList = [];
        vm.ansQuestion = ansQuestion;

        function listQn() {
            console.log("inside listQn")
            PeopleService
                .listQn()
                .then(function(result) {
                    console.log("listQnctrl " + result)
                    vm.questionList = result.data;
                })
                .catch(function(err) {
                    console.log("ListQn Ctrl err" + err)
                })

        } // end of listqn fn

        function ansQuestion(question_id) {
            console.log("listqnCtrl ans question ctrl");


            PeopleService
                .ansQuestion(question_id, attendance)

            .then(function(result) {
                    list();
                    console.log("result " + JSON.stringify(result));

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                });
        } // END function list()

        function deleteGuest(guest_email) {
            console.log("sending from ctrl delete attendance");
            console.log(guest_email);
            WedService
                .deleteGuest(guest_email)

            .then(function(result) {
                    list();
                    console.log("result " + JSON.stringify(result));

                })
                .catch(function(err) {
                    console.log("Ctrlerror " + err);
                });
        } // END function list()




    } //end of ListQnCtrl

})();