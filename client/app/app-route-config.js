(function() {
    angular
        .module("uirouterApp")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uirouterAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("index", {
                url: "/index",
                templateUrl: "index.html",

            })
            .state("login", {
                url: "/login",
                templateUrl: "app/users/login.html",

            })
            .state("post1Qn", {
                url: "/post1Qn",
                templateUrl: "app/post1qn/post1Qn.html",

            })
            .state("listquestions", {
                url: "/listquestions",
                templateUrl: "app/listquestions/listquestions.html",
                // controller: 'AppCtrl',
                // controllerAs: 'ctrl'
            })
            .state("List1QnMA", {
                url: "/List1QnMA",
                templateUrl: "app/List1QnMA/List1QnMA.html",
                // controller: 'AppCtrl',
                // controllerAs: 'ctrl'
            })
            .state("register", {
                url: "/register",
                templateUrl: "app/users/register.html"

            })
            .state("D", {
                url: "/D",
                templateUrl: "pages/people.html"
            })
            .state("E", {
                url: "/E/:personId",
                parent: 'D',
                templateUrl: "pages/person.html"
            })
            .state("F", {
                url: "/F",
                templateUrl: "pages/page4.html"
            })

        // $urlRouterProvider.otherwise("index");

    }

})();