(function() {
    angular
        .module("uirouterApp")
        .controller("LoginCtrl", LoginCtrl);

    LoginCtrl.$inject = ['$state','$http', 'PeopleService'];

    function LoginCtrl($state, $http, PeopleService) {
        var vm = this;

        vm.question_text = "";
        vm.login = login;
        vm.logout = logout;

        function login() {
            console.log("inside LoginCtrl")
            PeopleService
                .login(vm.user)
                .then(function(result) {
                    console.log("LoginCtrl " + result)
                    vm.questionList = result.data;
                })
                .catch(function(err) {
                    console.log("ListQn Ctrl err" + err)
                })

        } // end of login fn

        function logout() {
            console.log("inside LogoutCtrl")
            PeopleService
                .logout()
                .then(function(result) {
                    console.log("LoginCtrl " + result)
                    vm.questionList = result.data;
                })
                .catch(function(err) {
                    console.log("ListQn Ctrl err" + err)
                })

        } // end of logout fn
        
     




    } //end of ListQnCtrl

})();