(function() {
    angular
        .module('uirouterApp')
        .service('PeopleService', PeopleService);

    PeopleService.$inject = ['$http'];


    function PeopleService($http) {
        var service = this;

        service.post1Qn = post1Qn;
        service.listQn = listQn;
        service.List1QnMA = List1QnMA;
        
//auth functions
// function login() {
//             console.log("inside people service login");
//             return $http({
//                 method: 'GET',
//                 url: 'api/login'
//             });
//         }

// function logout() {
//             console.log("inside people service login");
//             return $http({
//                 method: 'GET',
//                 url: 'api/login'
//             });
//         }
        function post1Qn(question_text) {
            console.log("inside people service post1qn");
            return $http({
                method: 'POST',
                url: 'api/post1qn',
                data: {
                    question_text: question_text
                }
            });
        }

        function listQn() {
            console.log("inside people service listqn");
            return $http({
                method: 'GET',
                url: 'api/listqn'
            });
        }
 function List1QnMA() {
            console.log("inside people service List1QnMA");
            return $http({
                method: 'GET',
                url: '/api/List1QnMA'
            });
        }

        // var service = {
        //   getAllPeople: function () {
        //     console.log("get all people !");
        //     return $http.get('data/people.json', {
        //       cache: true
        //     }).then(function (resp) {
        //       console.log(resp.data);
        //       return resp.data;
        //     });
        //   },

        //   getPerson: function (id) {
        //     function personMatchesParam(person) {
        //       return person.id === id;
        //     }

        //     return service.getAllPeople().then(function (people) {
        //       return people.find(personMatchesParam)
        //     });
        //   }
        // }

        // return service;
    }
})();