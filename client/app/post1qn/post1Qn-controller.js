(function () {
    angular
        .module("uirouterApp")
        .controller("Post1QnCtrl",Post1QnCtrl);

Post1QnCtrl.$inject=['$state', '$http', 'PeopleService'];

    function Post1QnCtrl( $state, $http, PeopleService){
        var vm = this;

        vm.question_text = "";
            

        vm.post1Qn = post1Qn;
     
        function post1Qn(){
            console.log("inside post1Qn")
             console.log(vm.question_text)
            PeopleService
            .post1Qn(vm.question_text)
           
            .then(function(result){
                console.log("post1Qnctrl " + result)
                vm.question_text ="";
            })
            .catch(function(err){
                console.log("post1Qn err" + err)
            })

        } // end of post1qn fn

    }//end of Post1QnCtrl

})();