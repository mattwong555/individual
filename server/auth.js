'use strict';

var LocalStrategy = require('passport-local').Strategy;

var config = require('./config');

module.exports = function(app, passport) {
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password' 
  }, db.authenticateUser));

  passport.serializeUser(function(user, cb) {
    cb(null, user);
  });

  passport.deserializeUser(function(id, cb) {
    db.findByUsername(id, function(err, user) {
        if (err) { return cb(err); }
        cb(null, user);
    });
  });
};
