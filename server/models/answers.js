module.exports = function(conn, Sequelize) {
    var answers = conn.define('answers', {
        answer_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        answer_text: {
            type: Sequelize.STRING,
            allowNull: false,
        },
       question_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'questions',
                key: 'question_id'
            }
        },
    }, {
        tableName: 'answers',
        //freezeTable : true,
        timestamps: true
    });
    return answers;
};