module.exports = function(conn, Sequelize) {
    var users = conn.define('users', {
        user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false,
             
        },  
         password: {
                type: Sequelize.STRING,
                allowNull: false,

            }
        
    }, 
    
    
            //comment out here
        // conn.sync({
            // force: true
        // })
        // .then(function() {
            // console.log('Item table created successfully');
        // }),
        //comment out to here

    
    
    
    {
        tableName: 'users',
        //freezeTable : true,
        timestamps: true
    });
    return users;
};