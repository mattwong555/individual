module.exports = function(conn, Sequelize) {
    var levels = conn.define('levels', {
        level_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        level: {
            type: Sequelize.STRING,
            allowNull: false,
        },
 
    }, {
        tableName: 'levels',
        //freezeTable : true,
        timestamps: true
    });
    return levels;
};