module.exports = function(conn, Sequelize) {
    var students = conn.define('students', {
            student_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            student_email: {
                type: Sequelize.STRING,

                allowNull: false,
                validate: {
                    isEmail: true, // Back end email validation :)
                }
            },
            student_name: {
                type: Sequelize.STRING,
                allowNull: false,

            }

        },
        //comment out here
        // conn.sync({
            // force: true
        // })
        // .then(function() {
            // console.log('Item table created successfully');
        // }),
        //comment out to here

        {
            tableName: "students",
            timestamps: true,

        });
    return students;
};