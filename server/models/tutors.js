module.exports = function(conn, Sequelize) {
    var tutors = conn.define('tutors', {
        tutor_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
             
        },  
         tutor_name: {
                type: Sequelize.STRING,
                allowNull: false,

            }
        
    }, {
        tableName: 'tutors',
        //freezeTable : true,
        timestamps: true
    });
    return tutors;
};