module.exports = function(conn, Sequelize) {
    var questions = conn.define('questions', {
        question_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        question_text: {
            type: Sequelize.STRING,
            allowNull: false,
        }


        
    }, {
        tableName: 'questions',
        //freezeTable : true,
        timestamps: true
    });
    return questions;
};