// 'use strict';
// var express = require('express');
// var path = require('path');
// var bodyParser = require('body-parser');
// var Sequelize = require("sequelize");
// var session = require('express-session');
// var passport = require('passport');

// const MYSQL_USERNAME = "root";
// const MYSQL_PASSWORD = "qwerty";

// var conn = new Sequelize(
//   'tuition',
//   MYSQL_USERNAME,
//   MYSQL_PASSWORD, {
//     host: 'localhost',
//     logging: console.log,
//     dialect: 'mysql',
//     pool: {
//       max: 5,
//       min: 0,
//       idle: 10000
//     }
//   }
// );

var users = require('./models/users')(conn, Sequelize);

// // var users = require('./users');

exports.findByUsername = function (username, cb) {
  var isFound = false;
  users.forEach(function (user, idx) {
    if (username == user.username) {
      isFound = true;
      return cb(null, username);
    }
  });
  if (!isFound) {
    return cb(new Error("User '" + username + " does not exist"), null);
  }
};

exports.authenticateUser = function (username, password, done) {
  var isFound = false;
  users.forEach(function (user, idx) {
    if (username == user.username &&
      password == user.password) {
      isFound = true;
      return done(null, username);
    }
  });
  if (!isFound) {
    return done(null, false);
  }
};