    var express = require('express');
    var path = require('path');
    var bodyParser = require('body-parser');
    var Sequelize = require("sequelize");
    var session = require('express-session');
    var passport = require('passport');

    const MYSQL_USERNAME = "root";
    const MYSQL_PASSWORD = "qwerty";

//for login
    var config = require('./config');
    var db = require('./db');
    
    var conn = new Sequelize(
        'tuition',
        MYSQL_USERNAME,
        MYSQL_PASSWORD, {
            host: 'localhost',
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );



    var students = require('./models/students')(conn, Sequelize);
    var tutors = require('./models/tutors')(conn, Sequelize);
    var questions = require('./models/questions')(conn, Sequelize);
    var answers = require('./models/answers')(conn, Sequelize);
var users = require('./models/users')(conn, Sequelize);

    // var levels = require('./models/levels')(conn, Sequelize);


    // BEGIN: MYSQL RELATIONS

    // questions.hasMany(answers, {
    //    foreignKey: 'answer_id'
    // });

    //  questions.hasMany(answers);
    //  , {
    //    foreignKey: 'question_id'
    // });

      answers.belongsTo(questions
      , {
       foreignKey: 'question_id'
    });

    // qn1ansMany.belongsTo(answers, {
    //     foreignKey: 'answer_id',
    //     targetKey: 'answer_id'
    // });

    // qn1ansMany.hasOne(answers, {foreignKey: 'answer_id'});
    // questions.hasMany(qn1ansMany, {foreignKey: 'questions_id'});

    // END: MYSQL RELATIONS


    const NODE_PORT = process.env.NODE_PORT || 3000;

    
    const CLIENT_FOLDER = path.join(__dirname, '../client');
    const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

    var app = express();

    // setup of the configuration of express.
    app.use(express.static(CLIENT_FOLDER));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

//setting up local strategy
app.use(session({
    secret: config.session_secret,
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

require('./routes')(app, passport);
require('./auth')(app, passport);
// require('./auth')(db, app, passport);

//end set up local strategy

    /* start of GET PUT POST calls*/

    app.post("/api/post1qn", function(req, res) {
        console.log("Insert question");
        console.log(req.body);
        console.log(req.body.question_text);

        questions
            .create({
                question_text: req.body.question_text,

            }).then(function(question) {
                console.log(question);
                res.status(200).json(question);
            }).catch(function(err) {
                console.log("Create Error");
                res.status(500).json(err);
            });
    });

    app.get("/api/listqn", function(req, res) {
        console.log("list Question");

        conn.query("SELECT * FROM questions", { type: conn.QueryTypes.SELECT })
            .then(function(results) {
                console.log(results);
                res.status(202);
                res.json(results);
            })
            .catch(function(err) {
                console.log(err);
            });

    });

//  app.get("/api/list1QnManyAns", function(req, res) {
//         console.log("list 1Qn Many Ans");


//         conn.query("SELECT * FROM questions, answers WHERE question_id = answer_id", { type: conn.QueryTypes.SELECT })
//             .then(function(results) {
//                 console.log(results);
//                 res.status(202);
//                 res.json(results);
//             })
//             .catch(function(err) {
//                 console.log(err);
//             });

//     });

app.get("/api/List1QnMA", (req, res) => {  
    answers
    .findAll({
      include: [
        {
          model: questions,
          required: true,
          attributes: ['question_id','question_text']
        }
      ],
      attributes: ['answer_id','answer_text']
    })
    .then(function(answers) {
        console.log("List of answers:\n",answers.dataValues);
        console.log(answers);
        res.status(200).json(answers);
    })
    .catch(function(err) {
        console.log("Error: ",err);
    });
  });// end of apt get

exports.findByUsername = function (username, cb) {
  var isFound = false;
  users.forEach(function (user, idx) {
    if (username == user.username) {
      isFound = true;
      return cb(null, username);
    }
  });
  if (!isFound) {
    return cb(new Error("User '" + username + " does not exist"), null);
  }
};

exports.authenticateUser = function (username, password, done) {
  var isFound = false;
  users.forEach(function (user, idx) {
    if (username == user.username &&
      password == user.password) {
      isFound = true;
      return done(null, username);
    }
  });
  if (!isFound) {
    return done(null, false);
  }
};


    app.use(function(req, res) {
        res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
    });

    app.use(function(err, req, res, next) {
        res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
    });

    app.listen(NODE_PORT, function() {
        console.log("Server is running at port " + NODE_PORT);
    })